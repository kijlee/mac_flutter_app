import 'package:flutter/material.dart';
import 'package:mac_flutter_app/ui/main/MainPage.dart';
import 'package:mac_flutter_app/ui/page/PageRoutes.dart';

/**
 * 登陆界面
 * */
class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new Container(
//        width: 150,
//        height: 150,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/bg.png"), fit: BoxFit.cover),
        ),
//        margin: const EdgeInsets.all(4.0),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            title: Text("薪资宝"),
          ),
          body: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: 120),
              child: Column(
                children: <Widget>[
                  new Container(
//                      decoration: BoxDecoration(
//                        color: Colors.white,
//                        border: Border.all(color: Colors.black, width: 1.0),
//                        borderRadius: BorderRadius.all(Radius.circular(50.0)),
//                      ),
                    margin: new EdgeInsets.fromLTRB(0, 50, 0, 0),
                    padding: new EdgeInsets.all(10),
                    child: Image.asset(
                      'images/bg_logo.png',
                      width: 80.0,
                      height: 80.0,
                    ),
                  ),
                  new Container(
                    decoration: BoxDecoration(
                      color: Colors.white24,
                      border: Border.all(width: 1.0, color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                    child: new Stack(
                      alignment: new Alignment(1.0, 1.0),
                      children: <Widget>[
                        new TextField(
                          textAlign: TextAlign.left,
                          keyboardType: TextInputType.phone,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            hintText: '请输入用户名',
                            icon: new Icon(Icons.person),
//                        icon: new Image.asset('images/icon_username.png')
                          ),
                        ),
                        new IconButton(
                          icon: new Icon(Icons.clear, color: Colors.black45),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    decoration: BoxDecoration(
                      color: Colors.white24,
                      border: Border.all(width: 1.0, color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                    child: new Stack(
                      alignment: new Alignment(1.0, 1.0),
                      children: <Widget>[
                        new TextField(
                          keyboardType: TextInputType.phone,
                          textDirection: TextDirection.ltr,
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            hintText: '请输入密码',
                            icon: new Icon(Icons.enhanced_encryption),
                            //icon: new Image.asset('images/icon_username.png')
                          ),
                        ),
                        new IconButton(
                          icon: new Icon(Icons.clear, color: Colors.black45),
                        ),
                      ],
                    ),
                  ),
                  //登录按钮
                  new Container(
                    decoration: BoxDecoration(
                      color: Colors.lightBlue,
                      border: Border.all(width: 1.0, color: Colors.black),
                      borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    ),
                    margin: EdgeInsets.fromLTRB(30, 10, 30, 10),
                    padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0),
                    width: double.infinity,
                    child: MaterialButton(
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.pushNamed(context, PageRoutesBean.Main);
//                        Navigator.pushReplacement(
//                          context,
//                          new MaterialPageRoute(
//                              builder: (context) => MainPage()),
//                        );
                      },
                      child: new Text(
                        "登录",
                        style:
                            new TextStyle(fontSize: 16.0, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
