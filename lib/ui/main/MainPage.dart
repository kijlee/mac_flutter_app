import 'package:flutter/material.dart';
import 'package:mac_flutter_app/ui/interfaces/changeaccount.dart';

import 'BonusPage.dart';
import 'DrawerPage.dart';
import 'SalaryPage.dart';

class MainPage extends StatefulWidget {
String changeFlag='薪资宝';
MainPage({Key key,this.changeFlag}): super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _MainPageState();
  }
}

class _MainPageState extends State<MainPage> {
//  String accountFlag;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: '主页',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.deepOrange,
      ),
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          backgroundColor: Colors.grey[100],
          appBar: AppBar(
            title: Text('薪资宝'),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.message),
                tooltip: '消息',
                onPressed: () => debugPrint('Search button is pressed.'),
              )
            ],
            elevation: 0.0,
          ),
          body: SalaryAccount(),

          drawer: DrawerWeight(),

        ),
      ),
    );
  }

}
