import 'package:flutter/material.dart';
import 'package:mac_flutter_app/ui/interfaces/changeaccount.dart';

import 'MainPage.dart';

class DrawerWeight extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DrawerWeightState();
  }
}

class _DrawerWeightState extends State<DrawerWeight> {


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName:
                Text('姓名', style: TextStyle(fontWeight: FontWeight.bold)),
            accountEmail: Text("""职业/部门
                      手机号"""),
            currentAccountPicture: Image.asset(
              'images/bg_logo.png',
              width: 80.0,
              height: 80.0,
            ),
            decoration: BoxDecoration(
              color: Colors.yellow[400],
              image: DecorationImage(
                image: AssetImage("images/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/business.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new GestureDetector(
                    child: new Text(
                      '薪资宝',
                      style: TextStyle(color: Colors.deepOrangeAccent),
                    ),
                    onTap: (){},
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/star.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new GestureDetector(
                    child: new Text(
                      '奖金账户',
                      style: TextStyle(color: Colors.deepOrangeAccent),
                    ),
                    onTap: (){},
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/money.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new GestureDetector(
                    child: new Text(
                      '借款账户',
                      style: TextStyle(color: Colors.deepOrangeAccent),
                    ),
                    onTap: (){},
                  ),
                ),
              ],
            ),
          ),
          new Container(
            decoration: BoxDecoration(
                color: Colors.white,
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.5))),
            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
            child: new Text(
              '功能',
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/icon.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new Text(
                    '修改密码',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/people.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new Text(
                    '关于',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/delete.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new Text(
                    '注销',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(10, 12, 0, 12),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Image(
                  image: AssetImage("images/exit.png"),
                  width: 22,
                  height: 22,
                ),
                new Container(
                  padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new Text(
                    '退出程序',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


}
