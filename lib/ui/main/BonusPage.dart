import 'package:flutter/material.dart';

/**/
class BonusAccount extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BonusAccountState();
  }

}
class _BonusAccountState extends State<BonusAccount>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(minHeight: 120),
          child: Container(
            child: Column(
              children: <Widget>[
                //账户金额
                new Container(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 20),
                  decoration: BoxDecoration(color: Colors.deepOrangeAccent),
                  width: double.infinity,
                  child: Column(
                    children: <Widget>[
                      new Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: new Text(
                          "0.0",
                          style: TextStyle(
                            fontSize: 34,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      new Container(
                        child: new Text(
                          "奖金账户余额(元)",
                          style: TextStyle(fontSize: 14, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
                //账户资金收益
                new Container(
                  alignment: Alignment(-1.0, 0),
                  padding: EdgeInsets.fromLTRB(10, 8, 0, 8),
                  child: new Text(
                    '账户资金收益情况',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
                //昨日收益
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '昨日收益(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
                //本月累计收益
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '本月累计收益(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
                //上月收益
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '上月收益(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
                //账户总收益
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '账户总收益(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
                //用户操作详情
                new Container(
                  alignment: Alignment(-1.0, 0),
                  padding: EdgeInsets.fromLTRB(10, 8, 0, 8),
                  child: new Text(
                    '用户操作情况',
                    style: TextStyle(color: Colors.deepOrangeAccent),
                  ),
                ),
                //累计转入
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '累计转入(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
                //累计转出
                new Container(
                  padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                  alignment: Alignment(1.0, 0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(color: Colors.grey, width: 0.5))),
                  child: new Stack(
                    children: <Widget>[
                      new Align(
                        alignment: Alignment(-1.0, 0),
                        child: new Text(
                          '累计转出(元)',
                        ),
                      ),
                      new Align(
                        alignment: Alignment(1.0, 0),
                        child: new Text('0.0'),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: '搜索',
        child: Icon(Icons.search, color: Colors.white),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: new Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            new Container(
              padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
              child: new Text(
                '转出',
                style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 20),
              ),
            ),
            new Container(
              padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
              child: new Text(
                '转入',
                style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }
}