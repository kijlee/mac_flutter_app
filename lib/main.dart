import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:mac_flutter_app/ui/main/MainPage.dart';
import 'package:mac_flutter_app/ui/login/Login.dart';
import 'package:mac_flutter_app/ui/page/PageRoutes.dart';

import 'ui/interfaces/changeaccount.dart';

void main() {
  runApp(App());
  if (Platform.isAndroid) {
    // 以下两行 设置android状态栏为透明的沉浸。写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，写在渲染之前MaterialApp组件会覆盖掉这个值。
    //    SystemUiOverlayStyle systemUiOverlayStyle =
    //    SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    //    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      locale: Locale('zh','CN'),
        debugShowCheckedModeBanner:false,
//        showSemanticsDebugger:true,
      supportedLocales: [
        Locale('en','US'),
        Locale('zh','CN'),
      ],

      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

        // home: NavigatorDemo(),
        initialRoute: PageRoutesBean.Login,
        routes: {//界面路由
          PageRoutesBean.Login : (ctx)=> new LoginPage(),
          PageRoutesBean.Main : (ctx) => new MainPage(changeFlag:'薪资宝'),
        },
    );
  }


}


